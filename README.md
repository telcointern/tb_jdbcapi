# JDBC driver for telcobase

Telcobase에 접속 및 조작을 하기 위한 Interfaces 중에는 JDBC API가 있다.
JDBC API를 지원하기 위해 **JDBC driver를 구현**하고자 한다. 단 이때, DAL API를 사용하여야 한다.


### **JDBC (Java DataBase Connectivity)** ###


- 자바 프로그램 안에서 SQL을 실행하기 위해 데이터이스를 연결해주는 애플리케이션 인터페이스

- JDBC를 사용하면 데이터베이스에 접근하는 프로그램을 따로 만들 필요 없이 작성한 프로그램 내에서 SQL문을 데이터베이스로 전송 가능

- JDBC의 구조는 다음과 같음

![그림1.png](https://bitbucket.org/repo/yaG7xE/images/3202131879-%EA%B7%B8%EB%A6%BC1.png)

- JDBC는 다음과 같은 순서로 처리됨

1. JDBC 드라이버 로딩

2. 데이터베이스 connection 연결

3. 쿼리(sql)문장을 실행하기 위한 Statement / PreparedStatement / CallableStatement 객체 생성

4. 쿼리 실행

5. 쿼리 실행의 결과값(int, ResultSet) 사용

6. 사용된 객체 종료(ResultSet, Statement / PreparedStatement / CallableStatement , Connection)





### **Telcobase JDBC 구현 계획** ###


- 자바 애플리케이션에서 JDBC를 통한 DML 기능이 가능하도록 구현

- 구조는 다음과 같음

![그림2.png](https://bitbucket.org/repo/yaG7xE/images/815029992-%EA%B7%B8%EB%A6%BC2.png)


### **JNI (Java Native Interface)** ###


- Java와 C/C++ 모듈 간 인터페이스를 가능하게 하는 기능

- JVM에서 네이티브 함수는 C/C++로 구현되며 native 함수를 표시함으로써 자바로 구현되지 않는 특정 메소드들을 가지는 자바 클래스 선언

- JNI 프로토콜을 이용하여 네이티브 메소드를 C/C++ 함수로 작성

- C/C++ 함수를 컴파일 후 동적 라이브러리에 삽입하여 JVM이 동적 라이브러리를 호출

![그림3.png](https://bitbucket.org/repo/yaG7xE/images/4189301769-%EA%B7%B8%EB%A6%BC3.png)

- JNI 작동 순서는 다음과 같음

![그림4.png](https://bitbucket.org/repo/yaG7xE/images/528991753-%EA%B7%B8%EB%A6%BC4.png)


(명령어는 JNItest/Makefile 참조)