import com.telcobase.jdbc.*;
import java.sql.SQLException;
import java.util.*;
public class App {

	public static String menuSelect()
	{
		Scanner scan = new Scanner(System.in);
		String menu = null;	
		System.out.println("+---------------------------------+");
                System.out.println("| TELCOBASE JDBC TEST APPLICATION |");
                System.out.println("+---------------------------------+");
		System.out.println("|0. Exit                          |");
		System.out.println("|1. Excute an example of Insert   |");
		System.out.println("|2. Excute an example of Select   |");
		System.out.println("|3. Delete all from userinfo      |");
                System.out.println("|4. see memnory information       |");
		System.out.println("|5. Statement VS PreparedStatement|");
		System.out.println("|other. input a query             |");
		System.out.println("+---------------------------------+");
		System.out.print("input > ");
		menu = scan.nextLine();
		return menu;
	}
	public static void main(String[] args) {
		Insert exInsert = null;
		Select exSelect = null;
		Connection conn = null;
		PreparedStatement PStmt = null;
		Statement Stmt = null;
		ResultSet Rs = null;
		String sql = null;
		String menu = null;
		Scanner scan = new Scanner(System.in);
		try 
		{
			Class.forName("com.telcobase.jdbc.TBDriver");
		} 
		catch (ClassNotFoundException e) 
		{
			System.out.println("Driver loading failed: " + e);
		}
		try 
		{
			conn = DriverManager.getConnection();
		}
		catch (Exception e)
                {
                        e.printStackTrace();
                }
		
		while (true)
		{
			menu = menuSelect();
			if ( menu.equals("0") )
			{
				break;
			}
			else if ( menu.equals("1") )
			{
				try
				{
					sql = "insert into userinfo values (?, ?, ?, ?);";
       	                		PStmt = conn.prepareStatement(sql);
	
					System.out.println("create PreparedStatement successfully");
		
					int n = 1000;
					while ( n < 2000 )
					{
                	                	PStmt.setIntByIndex(1,n);
                                                PStmt.setStringByIndex(2,  "type_test");
                       		         	PStmt.setLongByIndex(3,  n+123456);
                   	    	       	  	PStmt.setDoubleByIndex(4,  n+0.5323);
                       		         	int rs = PStmt.executeUpdate();
                               		 	System.out.println("affect rows: " + rs);
		
                	                	n++;
					}
					conn.commit();
				}
				catch (Exception e)
                                {
                                        e.printStackTrace();
                                }
				PStmt.close();
                        }
			else if ( menu.equals("2") )
			{
				try 
				{
                        		sql = "select * from userinfo;";
                        		PStmt = conn.prepareStatement(sql);
                        		System.out.println("create PreparedStatement successfully");
                        		Rs = PStmt.executeQuery();
                        		while (Rs.next()) {
                                		System.out.println("minno:" + Rs.getInt("minno"));
                                		System.out.println("registry:" + Rs.getLong("registry"));
                                		System.out.println("balance:" + Rs.getDouble("balance"));
                                		System.out.println("name:" + Rs.getString("name"));
                        		}
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
                                }
				PStmt.close();
			}	
			else if ( menu.equals("3") )
                        {
				try
				{
                               		PStmt = conn.prepareStatement("delete from userinfo;");
					int rs1 = PStmt.executeUpdate();
                                	System.out.println("Query was executed:DELETE!");
				}
				catch (Exception e)
                                {
                                        e.printStackTrace();
                                }
				PStmt.close();
                        }
			else if ( menu.equals("4") )
			{
				conn.getMemInfo();
			}
			else if ( menu.equals("5") )
			{
				System.out.print("Input the number of data > ");
				String data = scan.nextLine();
				int number_of_data = Integer.parseInt(data);
				try
                                {
					//prepared statement
                                        sql = "insert into userinfo values (?, ?, ?, ?);";
					long pstmt_start = System.currentTimeMillis();
                                        PStmt = conn.prepareStatement(sql);

                                        int n = 1000;
                                        while ( n < (number_of_data+1000) )
                                        {
                                                PStmt.setIntByIndex(1,n);
                                                PStmt.setStringByIndex(2,  "type_test");
                                                PStmt.setLongByIndex(3,  n+123456);
                                                PStmt.setDoubleByIndex(4,  n+0.5323);
                                                int rs = PStmt.executeUpdate();

                                                n++;
                                        }
					long pstmt_end = System.currentTimeMillis();
					PStmt.close();
					
					//delete
					PStmt = conn.prepareStatement("delete from userinfo;");
                                        int rs1 = PStmt.executeUpdate();
	
					// statement
					long stmt_start = System.currentTimeMillis();
					n = 1000;
					while ( n < (number_of_data+1000) )
					{
						Stmt = conn.statement("insert into userinfo values (" + n + ", 'type_test' ," + (n+123456) + ", " + (n+0.5323) + ");");	
						int rs = Stmt.executeUpdate();
						n++;
					}
					long stmt_end = System.currentTimeMillis();

					long pstmt_time = pstmt_end - pstmt_start;
					long stmt_time = stmt_end - stmt_start;
					System.out.println("=========================================");
					System.out.println("Prepared Statement: " + pstmt_time + "ms");
					System.out.println("         Statement: " + stmt_time + "ms");
					System.out.println("=========================================");
                                }
                                catch (Exception e)
                                {
                                        e.printStackTrace();
                                }
				PStmt.close();
			}
			else
			{			
				System.out.println("----------------------------------------------------------");
				System.out.println("Examples)");
				System.out.println("select minno, registry, balance, name from userinfo;");
				System.out.println("insert into userinfo values(1000, 'test', null, 192033, 0.3421, null);");
				System.out.println("update userinfo set minno=10000 where minno=1000;");
				System.out.println("delete from userinfo");
				System.out.println("----------------------------------------------------------");
				System.out.print("SQL > ");
				sql = scan.nextLine();
				try
				{
					PStmt = conn.prepareStatement(sql);
					String[] words = sql.toLowerCase().split("\\s");
					System.out.println(words[0]);

					if ( words[0].equals("select") )
					{
						Rs = PStmt.executeQuery();
						while (Rs.next())
						{
							System.out.println("minno: " + Rs.getInt("minno"));
							System.out.println("registry: " + Rs.getLong("registry"));
							System.out.println("balance: " + Rs.getDouble("balance"));
							System.out.println("name: " + Rs.getString("name"));
						}
					}
					else
					{
						int rs = PStmt.executeUpdate();
						System.out.println("Query was executed!");
					}
				}
				catch (Exception e)
				{
                                	e.printStackTrace();
				}
				PStmt.close();
			}
		}
		if (conn != null)
		{
			try
			{
				System.out.println("Connection closed...");
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
        }
}
				
