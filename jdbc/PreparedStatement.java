package com.telcobase.jdbc;
import java.sql.SQLException;
import java.util.Date;

public class PreparedStatement //implements java.sql.PreparedStatement
{
	public long stmt_ptr;

	Connection conn=null;
	String sql=null;

        public PreparedStatement(Connection conn, String sql)
        {
		System.out.println("[PreparedStatement.java] Initialize PreparedStatement");
		System.out.println("[PreparedStatement.java] conn : " + conn);
		System.out.println("[PreparedStatement.java] sql : " + sql);
		this.conn = conn;
		this.sql = sql;
		stmt_ptr = init(conn.conn_ptr, sql);
        }
	public ResultSet executeQuery()
	{
		return new ResultSet(conn, stmt_ptr);	
	}
	public native ResultSet execute_Query();
        /*
	public ResultSet executeQuery()
	{
		System.out.println("[PreparedStatement.java] executeQuery");
		System.out.println("[PreparedStatement.java] JAVA_conn: " + conn);
		System.out.println("[PreparedStatement.java] JAVA_stmt: " + this);
		Object obj = preparedExecuteQuery(conn, this);
		ResultSet resultSet = null;
		return resultSet;
	}
	*/
	public native long init(long conn_ptr, String sql);
	public native void checkResultSet() throws SQLException;
	public native int executeUpdate() throws SQLException;
	public native void clearParameters() throws SQLException;
	public native void setIntByIndex(int papameterIndex, int x) throws SQLException;
	public native void setStringByIndex(int parameterIndex, String x) throws SQLException;
	public native void setLongByIndex(int parameterIndex, long x) throws SQLException;
	public native void setDoubleByIndex(int parameterIndex, double x) throws SQLException;
	public native void setBytesByIndex(int parameterIndex, byte x) throws SQLException;
	public native void setDateByIndex(int parameterIndex, Date x) throws SQLException;
	public native void setInt(String colName, int x) throws SQLException;
        public native void setString(String colName, String x) throws SQLException;
        public native void setLong(String colName, long x) throws SQLException;
        public native void setDouble(String colName, double x) throws SQLException;
        public native void setBytes(String colName, byte x) throws SQLException;
        public native void setDate(String colName, Date x) throws SQLException;
//	public native void setDateTime(int parameterIndex, Time x) throws SQLException;
//	public native void setTimestamp(int parameterIndex, Timestamp x) throws SQLException;
//	public native void setNull(int parameterIndex, int sqlType) throws SQLException;
//	public native void setNull(int parameterIndex, int sqlType, String typeName) throws SQLException;
//	public native ResultSetMetaData getMetaData() throws SQLException;
//	public native void setBlob(int i, Blob x) throws SQLException;
//	public native void setBinaryStream(int parameterIndex, InputStream X, int length) throws SQLException;
//	public native void setAsciiStream(int parameterIndex, InputStream X, int length) throws SQLException;
//	public native void setCharacterStream(int parameterIndex, Reader reader, int length) throws SQLException;
//	public native void BigDecimal (int parameterIndex, BigDecimal X) throws SQLException;
	public native void cancel() throws SQLException;
	public native void setByte(int parameterIndex, byte x) throws SQLException;
	public native void setBoolean(int parameterIndex, boolean x) throws SQLException;
	public native void setFloat(int parameterIndex, float x) throws SQLException;
//	public native void setObject(int parameterIndex, Obejct X) throws SQLException;
//	public native void setObject(int parameterIndex, Obejct X, int targetSqlType) throws SQLException;
//	public native void setObject(int parameterIndex, Obejct X, int targetSqlType, int scale) throws SQLException;
	public native void setShort(int parameterIndex, short x) throws SQLException;
//	public native void setClob(int i, Clob x) throws SQLException;
//	public native void setDate(int parameterIndex, Date X, Calendar cal) throws SQLException;
//	public native void setTime(int parameterIndex, Time X, Calendar cal) throws SQLException;
//	public native void setTimestamp(int parameterIndex, Timestamp X, Calendar cal) throws SQLException;
//	public native void setUnicodeStream(int parameterIndex, InputStream X, int length) throws SQLException;
	public native int preparedExecute() throws SQLException;
	public native ResultSet preparedExecuteQuery(Connection conn, PreparedStatement psmt);
	public native void close();
//	public native void setURL(int parameterIndex, URL x) throws SQLException;
	static
	{
		System.loadLibrary("preparedstatement");
	}
}
