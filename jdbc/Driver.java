package com.telcobase.jdbc;

public interface Driver {
	Connection getConnection();
}
