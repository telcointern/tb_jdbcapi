#include <jni.h>
#include "com_telcobase_jdbc_PreparedStatement.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dal.h>

#define DAL_ON 1
#define DAL_OFF 0

DAL_CONN *conn;
DAL_PSTMT *stmt;
char* query;

JNIEXPORT jlong JNICALL Java_com_telcobase_jdbc_PreparedStatement_init
(JNIEnv *env, jobject obj, jlong conn_ptr, jstring sql)
{
	query = (char*)malloc(sizeof(char)*1024);
	conn = (DAL_CONN*)(long)conn_ptr;
	const char *msg = (*env)->GetStringUTFChars(env, sql, NULL);
	strcpy(query, msg);
	stmt = dalPreparedStatement(conn, query);
	//strcpy(query, temp_query);
	
	printf("[PreparedStatementImp.c] dal_conn in pstmt: %p\n", conn);
	printf("[PreparedStatementImp.c] dal_string in pstmt: %s\n", query);

	return ((long)stmt);
}


JNIEXPORT jint JNICALL Java_com_telcobase_jdbc_PreparedStatement_executeUpdate
(JNIEnv *env, jobject obj)
{
	if ( dalPreparedExecUpdate(conn, stmt) < 0 )
	{
		fprintf(stderr, "can't execute a Prepared Statement, errno=%d\n", dalErrno());
		exit(-1);
	}
}

JNIEXPORT void JNICALL Java_com_telcobase_jdbc_PreparedStatement_setIntByIndex
(JNIEnv *env, jobject obj, jint parameterIndex, jint x)
{
	int registry = x;
	if ( dalSetIntByIndex(stmt, parameterIndex, registry) < 0 ) 
	{
		fprintf(stderr, "can't set an integer value, errno=%d\n", dalErrno());
		exit(-1);
	}
}

JNIEXPORT void JNICALL Java_com_telcobase_jdbc_PreparedStatement_setStringByIndex
(JNIEnv *env, jobject obj, jint parameterIndex, jstring x)
{
	char* username = (char*)malloc(sizeof(char)*1024);
	const char *convert_x = (*env)->GetStringUTFChars(env, x, NULL);
	strcpy(username, convert_x);
        if ( dalSetStringByIndex(stmt, parameterIndex, username) < 0 )
	{
                fprintf(stderr, "can't set an string value, errno=%d\n", dalErrno());
                exit(-1);
        }
}


JNIEXPORT void JNICALL Java_com_telcobase_jdbc_PreparedStatement_setLongByIndex
(JNIEnv *env, jobject obj, jint parameterIndex, jlong x)
{
        DAL_LONG registry = x;
        if ( dalSetLongByIndex(stmt, parameterIndex, &registry) < 0 ) 
	{	
                fprintf(stderr, "can't set a long value, errno=%d\n", dalErrno());
                exit(-1);
        }
}

JNIEXPORT void JNICALL Java_com_telcobase_jdbc_PreparedStatement_setDoubleByIndex
(JNIEnv *env, jobject obj, jint parameterIndex, jdouble x)
{
        DAL_DOUBLE balance = x;
        if ( dalSetDoubleByIndex(stmt, parameterIndex, &balance) < 0 ) 
	{
                fprintf(stderr, "can't set a double value, errno=%d\n", dalErrno());
                exit(-1);
        }
}
/*
JNIEXPORT void JNICALL Java_com_telcobase_jdbc_PreparedStatement_setDate
(JNIEnv *env, jobject obj, jint parameterIndex, jdate x)
{
        DAL_DATE last_login = x;
	if ( time(&last_login) < 0 ) 
	{
		fprintf(stderr, "can't get the value of current time, errno=%d\n", dalErrno());
                exit(-1);
        }
	if ( dalSetDateByIndex(stmt, parameterIndex, last_login) < 0 )
	{
		fprintf(stderr, "can't set a string value, errno=%d\n", darErrno());
		exit(-1);
	}
}
*/
JNIEXPORT void JNICALL Java_com_telcobase_jdbc_PreparedStatement_setInt
(JNIEnv *env, jobject obj, jstring colName, jint x)
{
        char convert_colName[100];
        strcpy(convert_colName, (*env)->GetStringUTFChars(env, colName, NULL));

        int registry = x;
        if ( dalSetIntByKey(stmt, convert_colName, registry) < 0 ) 
	{
                fprintf(stderr, "can't set an integer value, errno=%d\n", dalErrno());
                exit(-1);
        }
}

JNIEXPORT void JNICALL Java_com_telcobase_jdbc_PreparedStatement_setString
(JNIEnv *env, jobject obj, jstring colName, jstring x)
{
	char convert_colName[100];
        strcpy(convert_colName, (*env)->GetStringUTFChars(env, colName, NULL));

        char* username = (char*)malloc(sizeof(char)*1024);
        const char *convert_x = (*env)->GetStringUTFChars(env, x, NULL);
        strcpy(username, convert_x);
        if ( dalSetStringByKey(stmt, convert_colName, username) < 0 )
        {
                fprintf(stderr, "can't set an string value, errno=%d\n", dalErrno());
                exit(-1);
        }
}


JNIEXPORT void JNICALL Java_com_telcobase_jdbc_PreparedStatement_setLong
(JNIEnv *env, jobject obj, jstring colName, jlong x)
{
	char convert_colName[100];
        strcpy(convert_colName, (*env)->GetStringUTFChars(env, colName, NULL));

        DAL_LONG registry = x;
        if ( dalSetLongByKey(stmt, convert_colName, &registry) < 0 ) 
	{
                fprintf(stderr, "can't set a long value, errno=%d\n", dalErrno());
                exit(-1);
        }
}

JNIEXPORT void JNICALL Java_com_telcobase_jdbc_PreparedStatement_setDouble
(JNIEnv *env, jobject obj, jstring colName, jdouble x)
{
	char convert_colName[100];
        strcpy(convert_colName, (*env)->GetStringUTFChars(env, colName, NULL));

	double balance = x;
        if ( dalSetDoubleByKey(stmt, convert_colName, &balance) < 0 ) 
	{
                fprintf(stderr, "can't set a double value, errno=%d\n", dalErrno());
                exit(-1);
        }
}
/*
JNIEXPORT void JNICALL Java_com_telcobase_jdbc_PreparedStatement_setDate
(JNIEnv *env, jobject obj, jstring colName, jdate x)
{
	char convert_colName[100];
        strcpy(convert_colName, (*env)->GetStringUTFChars(env, colName, NULL));

        DAL_DATE last_login = x;
        if ( time(&last_login) < 0 )
        {
                fprintf(stderr, "can't get the value of current time, errno=%d\n", dalErrno());
                exit(-1);
        }
        if ( dalSetDateByKey(stmt, convert_colName, last_login) < 0 )
        {
                fprintf(stderr, "can't set a string value, errno=%d\n", darErrno());
                exit(-1);
        }
}
*/

JNIEXPORT void JNICALL Java_com_telcobase_jdbc_PreparedStatement_close
(JNIEnv *env, jobject obj)
{
	if ( dalDestroyPreparedStmt(stmt) < 0 )
	{
		fprintf(stderr, "can't destroy a Prepared Statement, errno=%d\n", dalErrno());
		exit(-1);
	}
}	
