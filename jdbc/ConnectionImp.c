#include <jni.h>
#include "com_telcobase_jdbc_Connection.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dal.h>
#include <signal.h>

#define DAL_ON 1
#define DAL_OFF 0

DAL_CONN *conn;
DAL_MEM_INFO *minfo;
DAL_SESSION_INFO *sinfo;
DAL_PSTMT *stmt;

JNIEXPORT jlong JNICALL Java_com_telcobase_jdbc_Connection_connect
(JNIEnv *env, jobject obj)
{
	printf("[ConnectionImp.c] dalConnect!!\n");
	conn = dalConnect(NULL);
	if ( conn == NULL )
        {
                fprintf (stderr, "can't connect to database, errno=%d\n", dalErrno());
                exit(-1);
        }
	printf("[ConnectionImp.c] connect ptr: %p\n", conn);
	return ((long) conn);
}

JNIEXPORT void JNICALL Java_com_telcobase_jdbc_Connection_getMemInfo
(JNIEnv *env, jobject obj)
{
	if ( (minfo = dalGetMemInfo(conn)) == NULL )
	{
		fprintf(stderr, "can't get memory information, errno=%d\n", dalErrno());
		exit(-1);
	}
	printf("-------Memory Information-------\n");
	printf("total = %s\nfree = %s\nallocted = %s\n", minfo->total, minfo->free, minfo->allocated);
}

JNIEXPORT void JNICALL Java_com_telcobase_jdbc_Connection_getSessionInfo
(JNIEnv *env, jobject obj)
{
        if ( (sinfo = dalGetSessionInfo(conn)) == NULL )
        {
                fprintf(stderr, "can't get session information, errno=%d\n", dalErrno());
                exit(-1);
        }
        //printf("Session ID=%s  time=%s  IP=%s  PID=%d\n", sinfo->session_id, sinfo->connected_time, sinfo->client_ip, sinfo->connector_pid);
}

JNIEXPORT jobject JNICALL Java_com_telcobase_jdbc_Connection_prepareStatement
(JNIEnv *env, jobject obj, jstring sql)
{
	printf("[ConnectionImp.c] prepareStatement method is start!");
	char query[256];
	const char *convert_sql = (*env)->GetStringUTFChars(env, sql, NULL);
	strcpy(query, convert_sql);
	stmt = dalPreparedStatement(conn, query);
	if ( stmt == NULL )
	{
		fprintf(stderr, "can't make a Propared Statement, query=%s errno=%d\n", query, dalErrno());
		return NULL;
	}
	printf("[ConnectionImp.c] prepareStatement method is complete!");
	//return stmt;			
}

JNIEXPORT int JNICALL Java_com_telcobase_jdbc_Connection_getAutoCommit
(JNIEnv *env, jobject obj, jboolean autoCommit)
{
	printf("Auto commit mode is %s.\n", (dalgetCommitMode(conn) == DAL_OFF)? "off":"on");
}


JNIEXPORT void JNICALL Java_com_telcobase_jdbc_Connection_setAutoCommit
(JNIEnv *env, jobject obj, jboolean autoCommit)
{
	if ( autoCommit ) {
		if ( dalSetAutoCommit(conn, DAL_ON) < 0 )
		{
			fprintf(stderr, "can't enable auto commit mode, errno=%d\n", dalErrno());
			exit (-1);
		}
	}
	else {
		if ( dalSetAutoCommit(conn, DAL_OFF) < 0 )
                {
                        fprintf(stderr, "can't enable auto commit mode, errno=%d\n", dalErrno());
                        exit (-1);
                }
        }
}

JNIEXPORT void JNICALL Java_com_telcobase_jdbc_Connection_commit
(JNIEnv *env, jobject obj)
{
	if ( dalCommit(conn) < 0 )
	{
		fprintf(stderr, "can't commit the transaction, errno=%d\n", dalErrno());
		exit (-1);
	}
	
}

JNIEXPORT void JNICALL Java_com_telcobase_jdbc_Connection_rollback
(JNIEnv *env, jobject obj)
{
	if ( dalRollback(conn) < 0)
	{
		fprintf(stderr, "can't rollback the transaction, errno=%d\n", dalErrno());
		exit (-1);
	}
}

JNIEXPORT void JNICALL Java_com_telcobase_jdbc_Connection_close
(JNIEnv *env, jobject obj)
{
	if ( dalDisconnect(conn) < 0 )
	{
		fprintf(stderr, "can't disconnect from database, errno=%d\n", dalErrno());
		exit (-1);
	}
}

JNIEXPORT jboolean JNICALL Java_com_telcobase_jdbc_Connection_isClosed
(JNIEnv *env, jobject obj)
{
	int ret = dalIsConnected(conn);
	if ( ret == 1 ) 
	{
		fprintf(stderr, "DB Connection is dead.\n");
	}
	return ret;
}


