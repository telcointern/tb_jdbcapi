package com.telcobase.jdbc;

import java.sql.*;
public class ResultSet //implements java.sql.ResultSet
{
	public long rs_ptr;
	public long entry_ptr;
	Connection conn;
	String sql;
	public ResultSet(Connection conn, String sql)
	{
		this.conn = conn;
		this.sql = sql;
		rs_ptr = init(conn.conn_ptr, sql);
		System.out.println("Result Set Conn.conn_ptr: " + conn.conn_ptr);
		System.out.println("Result Set sql: " + sql);
	}
	public ResultSet(Connection conn, long stmt_ptr)
        {
                this.conn = conn;
                rs_ptr = prepareInit(conn.conn_ptr, stmt_ptr);
                System.out.println("Result Set Conn.conn_ptr: " + conn.conn_ptr);
        }

	public boolean next()
	{
		entry_ptr = jnext(rs_ptr);
		return (entry_ptr==0)? false : true;
	}

	public boolean previous()
        {
                entry_ptr = jprevious(rs_ptr);
		return (entry_ptr==0)? false : true;
        }

	public boolean first()
        {
                entry_ptr = jfirst(rs_ptr);
		return (entry_ptr==0)? false : true;
        }

	public boolean last()
        {
                entry_ptr = jlast(rs_ptr);
		return (entry_ptr==0)? false : true;
        }

	public int getIntByIndex(int colIndex)
	{
		return jgetIntByIndex(colIndex, entry_ptr);
	}

        public long getLongByIndex(int colIndex) 
	{
                return jgetLongByIndex(colIndex, entry_ptr);
        }

        public double getDoubleByIndex(int colIndex)
	{
                return jgetDoubleByIndex(colIndex, entry_ptr);
        } 

        public String getStringByIndex(int colIndex)
	{
                return jgetStringByIndex(colIndex, entry_ptr);
        }
 
        public int getResultSetNumAttrs()
	{
                return jgetResultSetNumAttrs(rs_ptr);
        }

        public String getAttrNameByIndex(int colIndex)
	{
                return jgetAttrNameByIndex(colIndex, rs_ptr);
        }

	public int getInt(String colName)
	{
                return jgetInt(colName, entry_ptr);
        }
 
        public long getLong(String colName)
	{
                return jgetLong(colName, entry_ptr);
        }
 
        public double getDouble(String colName)
	{
                return jgetDouble(colName, entry_ptr);
        }
 
        public String getString(String colName)
	{
                return jgetString(colName, entry_ptr);
        } 

	public native long prepareInit(long conn_ptr, long stmt_ptr);
	public native long init(long conn_ptr, String sql);
	public native void excute() throws SQLException;
	public native long jnext(long rs_ptr);
	public native long jprevious(long rs_ptr);
	public native long jfirst(long rs_ptr);
	public native long jlast(long rs_ptr);
	public native int jgetIntByIndex(int colIndex, long entry_ptr);
	public native long jgetLongByIndex(int colIndex, long entry_ptr);
	public native double jgetDoubleByIndex(int colIndex, long entry_ptr);
	public native String jgetStringByIndex(int colIndex, long entry_ptr);
	public native int jgetResultSetNumAttrs(long rs_ptr);
	public native String jgetAttrNameByIndex(int colIndex, long rs_ptr);
//	public native byte getByteByIndex(int colIndex) throws SQLException;
	public native void clear() throws SQLException;
	public native boolean wasNull() throws SQLException;
	//public native ResultSetMetaData getMetaData() throws SQLException;
	public native int jgetInt(String colName, long entry_ptr);
	public native long jgetLong(String colName, long entry_ptr);
	public native double jgetDouble(String colName, long entry_ptr);
	public native String jgetString(String colName, long entry_ptr);
//	public native byte getBytesByKey(String colName) throws SQLException;
//	public native Date getDateByIndex(int colIndex) throws SQLException;
//	public native Date getDateByKey(String colName) throws SQLException;
//	public native Time getTimeByIndex(int colIndex) throws SQLException;
//	public native Time getTimeByKey(String colName) throws SQLException;
	public native void refreshRow() throws SQLException;
	
	static
	{
		System.loadLibrary("resultset");
	}		
}
