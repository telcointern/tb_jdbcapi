package com.telcobase.jdbc;

import java.sql.SQLException;

public class Connection {
	public long conn_ptr;
	public PreparedStatement prepareStatement(String Sql) 
	{
		return new PreparedStatement(this, Sql);
	}
	public Statement statement(String Sql)
	{
		return new Statement(this, Sql);
	}
	//  public native int getAutoCommit() throws SQLException;
    	public native long connect();
	public native void getMemInfo();
	public native void getSessionInfo();
	public native void setAutoCommit(boolean autoCommit) throws SQLException;
    	public native void commit() throws SQLException;
    	public native void rollback() throws SQLException;
    	public native void close() throws SQLException;
    	public native boolean isClosed() throws SQLException;
	//  public native DatabaseMetaData getMetaData() throws SQLException;
    	public native int getTransactionIsolation() throws SQLException;
    	public native void setTransactionIsolation() throws SQLException;
	
	public Connection()
        {
                System.out.println("[Connection.java] open Connection!");
		conn_ptr = connect();
		System.out.println("[Connection.java] conn_ptr: " + conn_ptr);
        }
	static
	{
		System.loadLibrary("connection");
	}
}
