#include <jni.h>
#include "com_telcobase_jdbc_Statement.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dal.h>

#define DAL_ON 1
#define DAL_OFF 0

DAL_CONN *conn;
char* query;

JNIEXPORT void JNICALL Java_com_telcobase_jdbc_Statement_init
(JNIEnv *env, jobject obj, jlong conn_ptr, jstring sql)
{
	query = (char*)malloc(sizeof(char)*1024);
	conn = (DAL_CONN*)(long)conn_ptr;
	const char *msg = (*env)->GetStringUTFChars(env, sql, NULL);
	strcpy(query, msg);
	//printf("[StatementImp.c] dal_conn in stmt: %p\n", conn_ptr);
	//printf("[StatementImp.c] dal_string in stmt: %s\n", query);
}


JNIEXPORT jint JNICALL Java_com_telcobase_jdbc_Statement_executeUpdate
(JNIEnv *env, jobject obj)
{
	int result = dalExecUpdate(conn, query);
	if ( result < 0 )
	{
		fprintf(stderr, "can't execute a Prepared Statement, errno=%d\n", dalErrno());
		exit(-1);
	}
	return result;
}
