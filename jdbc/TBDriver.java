package com.telcobase.jdbc;
import java.sql.*;
public class TBDriver implements Driver {
	private static Driver defaultDriver;
	static {
		System.out.println("static field");
		defaultDriver = new TBDriver();
		com.telcobase.jdbc.DriverManager.registerDefaultPrivider(defaultDriver);
	}
	
	@Override
	public Connection getConnection() {
		System.out.println("TBDriver's Connection return");
		return new Connection();
	}
}
