package com.telcobase.jdbc;
import java.sql.SQLException;

public class Statement //implements java.sql.PreparedStatement
{
	public long stmt_ptr;
	public Connection conn=null;
	public String sql=null;
        public Statement(Connection conn, String sql)
        {
		this.conn = conn;
		this.sql = sql;
		//System.out.println("[Statement.java] Initialize PreparedStatement");
		//System.out.println("[Statement.java] conn: " + this.conn);
		//System.out.println("[Statement.java] sql: " + this.sql);
		init(conn.conn_ptr, sql);
        }
	public ResultSet executeQuery()
	{
		return new ResultSet(conn, sql);	
	}
	public native void init(long conn_ptr, String sql);
	public native void checkResultSet() throws SQLException;
	public native int executeUpdate() throws SQLException;
	public native void close() throws SQLException;
	public native void clearParameters() throws SQLException;
//	public native ResultSetMetaData getMetaData() throws SQLException;
//	public native void BigDecimal (int parameterIndex, BigDecimal X) throws SQLException;
	public native void cancel() throws SQLException;
	static
	{
		System.loadLibrary("statement");
	}
}
