#include <jni.h>
#include "com_telcobase_jdbc_ResultSet.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dal.h>

DAL_CONN *conn;
DAL_PSTMT *stmt;
char query[255];

JNIEXPORT jlong JNICALL Java_com_telcobase_jdbc_ResultSet_jfirst
(JNIEnv *env, jobject obj, jlong rs_ptr)
{
	DAL_RESULT_SET *result = (DAL_RESULT_SET*)(long)rs_ptr;
	DAL_ENTRY *entry = dalFetchFirst(result);
	if ( entry == NULL )
	{
		fprintf(stderr, "can't fetch the first enrty from result set, errno=%d\n", dalErrno());
		return 0;
	}
	return ((long) entry);
}

JNIEXPORT jlong JNICALL Java_com_telcobase_jdbc_ResultSet_jlast
(JNIEnv *env, jobject obj, jlong rs_ptr)
{
	DAL_RESULT_SET *result = (DAL_RESULT_SET*)(long)rs_ptr;
        DAL_ENTRY *entry = dalFetchLast(result);
        if ( entry == NULL )
        {
                fprintf(stderr, "can't fetch the last enrty from result set, errno=%d\n", dalErrno());
                return 0;
        }
	return ((long) entry);
}

JNIEXPORT jlong JNICALL Java_com_telcobase_jdbc_ResultSet_jprevious
(JNIEnv *env, jobject obj, jlong rs_ptr)
{
	DAL_RESULT_SET *result = (DAL_RESULT_SET*)(long)rs_ptr;
        DAL_ENTRY *entry = dalFetchPrev(result);
        if ( entry == NULL )
        {
                fprintf(stderr, "can't fetch the previous enrty from result set, errno=%d\n", dalErrno());
                return 0;
        }
        return ((long) entry);
}

JNIEXPORT jlong JNICALL Java_com_telcobase_jdbc_ResultSet_jnext
(JNIEnv *env, jobject obj, jlong rs_ptr)
{
	DAL_RESULT_SET *result = (DAL_RESULT_SET*)(long)rs_ptr;
        DAL_ENTRY *entry = dalFetchNext(result);
        if ( entry == NULL )
        {
                fprintf(stderr, "can't fetch the next enrty from result set, errno=%d\n", dalErrno());
                return 0;
        }
        return ((long) entry);
}

JNIEXPORT jlong JNICALL Java_com_telcobase_jdbc_ResultSet_prepareInit
(JNIEnv *env, jobject obj, jlong conn_ptr, jlong stmt_ptr)
{
        conn = (DAL_CONN*)(long)conn_ptr;
	stmt = (DAL_PSTMT*)(long)stmt_ptr;
        DAL_RESULT_SET *result = dalPreparedExecQuery(conn, stmt);

	printf("[ResultSetImp.c] dal_conn in result set: %p\n", conn_ptr);
	printf("[ResultSetImp.c] dal_string in result set: %s\n", query);
	printf("[ResultSetImp.c] dal_pstmt in result set: %p\n", stmt_ptr);
	printf("[ResultSetImp.c] dal_result_set in result set: %p\n", result);
        if ( result == NULL )
        {
                fprintf(stderr, "can't execute query, errno=%d\n", dalErrno());
                exit(-1);
        }
        else
	{
                printf("Entry is not null :)\n");
        }
	return ((long) result);
}

JNIEXPORT jlong JNICALL Java_com_telcobase_jdbc_ResultSet_init
(JNIEnv *env, jobject obj, jlong conn_ptr, jstring sql)
{
	conn = (DAL_CONN*)(long)conn_ptr;
	const char *msg = (*env)->GetStringUTFChars(env, sql, NULL);
        strcpy(query, msg);
	DAL_RESULT_SET *result = dalExecQuery(conn, query);
	printf("[ResultSetImp.c] dal_conn in result set: %p\n", conn_ptr);
        printf("[ResultSetImp.c] dal_string in result set: %s\n", query);
	printf("[ResultSetImp.c] dal_result_set in result set: %p\n", result);
	if ( result == NULL )
        {
                fprintf(stderr, "can't execute query, errno=%d\n", dalErrno());
                exit(-1);
        }		
	else 
	{
		printf("Entry is not null :)\n");
	}

	printf("[ResultSetImp.c] conn_ptr: %p\n", conn_ptr);
	printf("[ResultSetImp.c] query: %s\n", query);
	return ((long) result);
}

JNIEXPORT jint JNICALL Java_com_telcobase_jdbc_ResultSet_jgetInt
(JNIEnv *env, jobject obj, jstring colName, jlong entry_ptr)
{
	DAL_ENTRY *entry = (DAL_ENTRY*)(long)entry_ptr;
	int minno =0 ;
	char convert_colName[100];
	strcpy(convert_colName, (*env)->GetStringUTFChars(env, colName, NULL));
	printf("convert_colName: %s\n", convert_colName);
	if ( dalGetIntByKey(entry, convert_colName, &minno) < 0 )
	{
		fprintf(stderr, "can't get a integer value from result entry, errno=%d\n", dalErrno());
		exit(-1);
	}
	return minno;
}

JNIEXPORT jlong JNICALL Java_com_telcobase_jdbc_ResultSet_jgetLong
(JNIEnv *env, jobject obj, jstring colName, jlong entry_ptr)
{
	DAL_ENTRY *entry = (DAL_ENTRY*)(long)entry_ptr;
	DAL_LONG registry;
	char convert_colName[100];
        strcpy(convert_colName, (*env)->GetStringUTFChars(env, colName, NULL));

        if ( dalGetLongByKey(entry, convert_colName, &registry) < 0 )
        {
                fprintf(stderr, "can't get a long value from result entry, errno=%d\n", dalErrno());
                exit(-1);
        }
	return registry;
} 

JNIEXPORT jdouble JNICALL Java_com_telcobase_jdbc_ResultSet_jgetDouble
(JNIEnv *env, jobject obj, jstring colName, jlong entry_ptr)
{
	DAL_ENTRY *entry = (DAL_ENTRY*)(long)entry_ptr;
        DAL_DOUBLE balance;
	char convert_colName[100];
        strcpy(convert_colName, (*env)->GetStringUTFChars(env, colName, NULL));

        if ( dalGetDoubleByKey(entry, convert_colName, &balance) < 0 )
        {
                fprintf(stderr, "can't get a double value from result entry, errno=%d\n", dalErrno());
                exit(-1);
        }
	return balance;
}

JNIEXPORT jstring JNICALL Java_com_telcobase_jdbc_ResultSet_jgetString
(JNIEnv *env, jobject obj, jstring colName, jlong entry_ptr)
{
	DAL_ENTRY *entry = (DAL_ENTRY*)(long)entry_ptr;
	char name[1024];
	DAL_STRING name_s = name;
	char convert_colName[100];
        strcpy(convert_colName, (*env)->GetStringUTFChars(env, colName, NULL));

        if ( dalGetStringByKey(entry, convert_colName, &name_s) < 0 )
        {
                fprintf(stderr, "can't get a string value from result entry, errno=%d\n", dalErrno());
                exit(-1);
        }
	jstring result = (*env)->NewStringUTF(env, name_s);
	return result;
}

JNIEXPORT jint JNICALL Java_com_telcobase_jdbc_ResultSet_jgetIntByIndex
(JNIEnv *env, jobject obj, jint colIndex, jlong entry_ptr)
{
	DAL_ENTRY *entry = (DAL_ENTRY*)(long)entry_ptr;
	int minno;
        if ( dalGetIntByIndex(entry, colIndex, &minno) < 0 )
        {
                fprintf(stderr, "can't get an integer value from result entry, errno=%d\n", dalErrno());
                exit(-1);
        }
	return minno;
}

JNIEXPORT jlong JNICALL Java_com_telcobase_jdbc_ResultSet_jgetLongByIndex
(JNIEnv *env, jobject obj, jint colIndex, jlong entry_ptr)
{
	DAL_ENTRY *entry = (DAL_ENTRY*)(long)entry_ptr;
	DAL_LONG registry;
        if ( dalGetLongByIndex(entry, colIndex, &registry) < 0 )
        {
                fprintf(stderr, "can't get a long value from result entry, errno=%d\n", dalErrno());
                exit(-1);
        }
	return registry;
}

JNIEXPORT jdouble JNICALL Java_com_telcobase_jdbc_ResultSet_jgetDoubleByIndex
(JNIEnv *env, jobject obj, jint colIndex, jlong entry_ptr)
{
	DAL_ENTRY *entry = (DAL_ENTRY*)(long)entry_ptr;
        DAL_LONG balance;
        if ( dalGetLongByIndex(entry, colIndex, &balance) < 0 )
        {
                fprintf(stderr, "can't get a double value from result entry, errno=%d\n", dalErrno());
                exit(-1);
        }
	return balance;
}

JNIEXPORT jstring JNICALL Java_com_telcobase_jdbc_ResultSet_jgetStringByIndex
(JNIEnv *env, jobject obj, jint colIndex, jlong entry_ptr)
{
	DAL_ENTRY *entry = (DAL_ENTRY*)(long)entry_ptr;
	char name_temp[1024];
        DAL_STRING name = name_temp;
        if ( dalGetStringByIndex(entry, colIndex, &name) < 0 )
        {
                fprintf(stderr, "can't get a string value from result entry, errno=%d\n", dalErrno());
                exit(-1);
        }
	jstring resultString = (*env)->NewStringUTF(env, name);
	return resultString;
}

JNIEXPORT jint JNICALL Java_com_telcobase_jdbc_ResultSet_jgetResultSetNumAttrs
(JNIEnv *env, jobject obj, jlong rs_ptr)
{
	DAL_RESULT_SET *result = (DAL_RESULT_SET*)(long)rs_ptr;
        int num_columns = dalGetResultSetNumAttrs(result);
	return num_columns;	
}

JNIEXPORT jstring JNICALL Java_com_telcobase_jdbc_ResultSet_jgetAttrNameByIndex
(JNIEnv *env, jobject obj, jint colIndex, jlong rs_ptr)
{
	DAL_RESULT_SET *result = (DAL_RESULT_SET*)(long)rs_ptr;
	char attr_name_temp[1024];
 	char* attr_name = attr_name_temp;     
        strcpy(attr_name, dalGetAttrNameByIndex(result, colIndex));

	jstring resultString = (*env)->NewStringUTF(env, attr_name);
        return resultString;	
}

