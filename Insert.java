import com.telcobase.jdbc.*;
import java.sql.SQLException;

public class Insert {

	public Insert() {
		Connection conn = null;
		PreparedStatement PStmt = null;
                String Str = null;
		try {
			Class.forName("com.telcobase.jdbc.TBDriver");
			System.out.println("Driver loaded successfully");
			
		} catch (ClassNotFoundException e) {
			System.out.println("Driver loading failed: " + e);
		}
		
		try {
			conn = DriverManager.getConnection();
			System.out.println("Connected successfully");
			
			Str = "insert into userinfo values (?, ?, ?, ?, ?, ?);";
			PStmt = conn.prepareStatement(Str);

			System.out.println("create PreparedStatement successfully");
			
			int n = 1000;
			while ( n < 2000 ) {
				PStmt.setIntByIndex(1,n);
				PStmt.setLongByIndex(4,  n+123456);
				PStmt.setDoubleByIndex(5,  n+0.5323);
				PStmt.setStringByIndex(2,  "type_test");
				int rs = PStmt.executeUpdate();
				System.out.println("affect rows: " + rs);
				
				n++;
			}
			
			PStmt.close();
			
			conn.commit();
		} catch(SQLException e) {
			System.out.println(e);
		} finally {
			if ( conn != null ) {
				try {
					conn.close();
					System.out.println("Connection closed...");
				}
				catch (SQLException e) {
					System.out.println(e);
				}
			}
		}
	}
}

