import com.telcobase.jdbc.*;
import java.sql.SQLException;
public class Select {

	public Select() {
		Connection conn = null;
		PreparedStatement PStmt = null;
		String Str = null;
		try {
			Class.forName("com.telcobase.jdbc.TBDriver");
			System.out.println("Driver loaded successfully");
		} catch (ClassNotFoundException e) {
			System.out.println("Driver loading failed: " + e);
		}
		try {
			conn = DriverManager.getConnection();
			if ( conn == null )
				System.out.println("conn is null!!");
			Str = "select * from userinfo;";
			PStmt = conn.prepareStatement(Str);
			System.out.println("create PreparedStatement successfully");
		}
		catch(Exception e) 
		{
			System.out.println(e);
		}
		
		try {
			System.out.println("ResultSet Rs");
			ResultSet Rs = PStmt.executeQuery();
			System.out.println("Pstmt.executeQuery");
			while (Rs.next()) {
				System.out.println("a:" + Rs.getInt("minno"));
				System.out.println("b:" + Rs.getLong("registry"));
				System.out.println("c:" + Rs.getDouble("balance"));
				System.out.println("d:" + Rs.getString("name"));
			}
			System.out.println("Not more tuple!!");
		//	Rs.close();
			PStmt.close();
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
					System.out.println("Connection closed...");
				} catch (SQLException e) {
					System.out.println(e);
				}
			}
		}
	
	}

}

