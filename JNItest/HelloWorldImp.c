#include <jni.h>
#include "com_jni_HelloWorld.h"
#include <stdio.h>
#include <string.h>
 

JNIEXPORT jstring JNICALL Java_com_jni_HelloWorld_displayHelloWorld(JNIEnv *env, jobject obj, jstring jstr)
{
	const char *name = (*env)->GetStringUTFChars(env, jstr, NULL);//Java String to C Style string
	char msg[60] = "Hello ";
	jstring result;

	strcat(msg, name);
	if ( !strcmp(name, "Jone") ) {
		strcat(msg, "\nYou are my BEST FRIEND.");
	}
	else {
		strcat(msg, "\nNice to see you.");
	}
	(*env)->ReleaseStringUTFChars(env, jstr, name);
	result = (*env)->NewStringUTF(env, msg); // C style string to Java String
	return result;
}

JNIEXPORT jint JNICALL Java_HelloWorld_addHello(JNIEnv *env, jobject obj, jint a, jint b)
{
	printf("Hello JAVA! This is C.\n");
	return a+b;
}
