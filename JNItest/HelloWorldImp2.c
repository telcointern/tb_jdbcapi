#include <jni.h>
#include "com_jni_HelloWorld2.h"
#include <stdio.h>
 

JNIEXPORT jint JNICALL Java_com_jni_HelloWorld2_pow
(JNIEnv *env, jobject obj, jint a, jint b)
{
	int i=0;
	int result=1;
	for (i=0; i<b; i++ )
	{
		result *= a;
	}	
	return result;
}
