package com.jni;

public class HelloWorld
{
	public native String displayHelloWorld(String name);
	public native int addHello(int a, int b);

	/*
	static
	{
		System.loadLibrary("hello");
	}
   	*/
	public static void main(String[] args) {
		com.jni.HelloWorld test = new com.jni.HelloWorld();
		String result = test.displayHelloWorld("Jone");
		System.out.println(result);
   	}

	       
	static
        {
                System.loadLibrary("hello");
        }
}
